import Eos from 'eosforce'

export const wait_time = (_time = 1) => {
  return new Promise((rs, rj) => {
    let t = setTimeout(() => {
      clearTimeout(t);
      rs();
    }, _time * 1000);
  });
}


export const transfer_for_update_ethaddr = async (eos, account_name, to, permission) => {
  let token = await eos.contract('eosio').then(token => { return token }),
        auth = {
            actor: account_name,
            permission
        };
    console.log(auth);
    return await token.transfer(account_name, to, '0.0000 EOS', 'update eth_addr', auth)
                .catch(error => {
                   throw error;
                })
}