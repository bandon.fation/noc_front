export default {
  en: [
    {
      id: 1,
      is_close: false,
      title: 'EOSForce Mainnet Introduction',
      list: [
        {
          id: 1.1,
          title: 'EOSForce Introduction',
          list: [
            'EOSForce is committed to exploring a more open crypto economy infrastructure in practice. Through the continuous development of multi-chain architecture blockchain protocol, it meets the diversified consensus requirements of the crypto economy and promotes the application of blockchain technology in various fields.',
            'EOSForce.io, the most active parallel mainnet in EOSIO ecosystem, has been completed and launched, providing more user-friendly resource model, richer underlying support for developers and fairer governance mechanism for the community. The EOSForce.io mainnet asset can be activated by offline signing with the mapped EOSIO private key.'
          ]
        },
        {
          id: 1.2,
          title: 'What is EOSC? Is EOSC an airdrop token?',
          list: [
            'EOSC is the token of an EOS mainnet launched by EOSForce community. It represents the assets on the EOSForce main chain and can be used for voting rewards, block generating, transferring and trading on exchanges.',
            'EOSC is not an airdrop token. Users can activate the EOSC account assets by offline signing with the EOSIO private key mapped before June 3, 2018 Beijing time.'
          ]
        },
        {
          id: 1.3,
          title: 'The difference between EOSC and EOS',
          list: [
            'EOSC is the token of the EOS mainnet launched by EOSForce. EOS is the token of the EOS mainnet launched by EMLG. These two assets are different. Currently, AKdex.io Exchange has listed the EOS and EOSC trading pairs.'
          ]
        },
        {
          id: 1.4,
          title: 'Total amount of EOSC (Including the lock-up plan, and allocation of the rest part)',
          list: [
            'The initial circulation of EOSC is 1 billion, and 94.6 million EOSC are issued annually to reward 23 BPs. The BlockOne team will be awarded a prize of 100 million EOSC unlocked every 10 years. And it will continue to do so without interruption.',
            'According to the proposal of EOSForce mainnet unactivated genesis account lockup plan, on December 20, the unactivated EOSIO genesis account will be locked with 80% of EOSC tokens, and replaced for the same amount of lock tokens. The remaining 20% can be freely used. Token increased annually will be adjusted appropriately according to the existing total amount of EOSC tokens in the mainnet.',
            'At present, the number of EOSC locked is 672,390,243, and the remaining is 327,609,757.'
          ]
        },
        {
          id: 1.5,
          title: 'Difference between EOSForce mainnet and EMLG mainnet',
          list: [
            'There is more than one EOS mainnet. Currently, the EOS community has launched several mainnets. The mainnet launched by EOSForce community is called EOSForce mainnet and is parallel to the mainnet launched by EMLG.',
            'When debugging EOS software, we found many unstable factors and there are certain asset security risks. Since EOS itself is a set of open source software, the official does not guarantee security and reliability of the blockchain launched by EOS software. This requires dedicated team for targeted development and testing to maintain the entire blockchain. Therefore, our team carried out strict tests on each function of EOSIO, upgraded and optimized the official codes, prioritized the security and reliability of the whole blockchain, and officially launched the EOSForce mainnet on June 22, 2018.',
            'EOSForce mainnet added the innovation of voting rewards incentive mechanism, improved the decentralization of one-token-one-vote, charged transaction fees to prevent DDOS attack, optimized the community governance of 23 BPs and introduced the new resource models.'
          ]
        },
        {
          id: 1.6,
          title: 'Why does EOSForce mainnet change 1-token-30-votes to 1-token-1-vote?',
          list: [
            '1-token-30-votes will cause the block producers to form alliances and vote each other, which will seriously affect the decentralization of the blockchain. Such monopoly will bring great harm to the EOS community, while 1-token-1-vote of the EOSForce mainnet can effectively avoid this situation and prevent the mainnet from falling into the centralized governance of the Qatar alliance.',
            'Block One CEO Brendan Blumer also suggested that EOSIO changes to one-token-one-vote in a telegram group on January 2019.',
          ]
        },
        {
          id: 1.7,
          title: 'Is this EOSC EOSClassic?',
          list: [
            'The full name of EOSC is EOS COIN, which is short for the mainnet token of EOSForce, not EOSClassic.'
          ]
        }
      ]
    },
    {
      id: 2,
      is_close: false,
      title: 'EOSForce accounts creating and transaction making',
      list: [
        {
          id: 2.1,
          title: 'How to create an EOSForce account?',
        },
        {
          id: 2.11,
          title: 'EOS Genesis users: using the following wallets by importing the EOS private key of mapping can get the EOSForce account.',
          list: [
            'EOSForce official wallet (PC version) — EOSForce.io',
            'International well-known wallet - Scatter',
            'International well-known wallet - Math Wallet',
            'Famous wallet in EOS ecosystem – Awake Wallet'
          ]
        },
        {
          id: 2.12,
          title: 'EOS non-genesis users: can create an EOSForce account in the following wallet',
          list: [
            'EOSForce official wallet (PC version) — EOSForce.io',
            'International well-known wallet - Math Wallet',
            'Famous wallet in EOS ecosystem – Awake Wallet',
          ]
        },
        {
          id: 2.2,
          title: 'Which wallets support EOSC?',
          list: [
            'Tutorial for activating EOSC using Scatter wallet:',
            '<div class="code"><a href="https://bbs.eosforce.io/topic/81/" target="_blank">https://bbs.eosforce.io/topic/81/</a></div>',
            'Scatter wallet download address:',
            '<div class="code"><a href="https://get-scatter.com/" target="_blank">https://get-scatter.com/</a></div>',
            'EOSForce Wallet tutorial:',
            '<div class="code"><a href="https://mp.weixin.qq.com/s/jbRRAmhoCKgHlga2V7xsDw" target="_blank">https://mp.weixin.qq.com/s/jbRRAmhoCKgHlga2V7xsDw</a></div>',
            'EOSForce Wallet download address:',
            '<div class="code"><a href="https://www.eosforce.io/" target="_blank">https://www.eosforce.io/</a></div>',
            'Math Wallet tutorial:',
            '<div class="code"><a href="https://mp.weixin.qq.com/s/QV0gQYfWV6xVIXpmtxpDOg" target="_blank">https://mp.weixin.qq.com/s/QV0gQYfWV6xVIXpmtxpDOg</a></div>',
            'Math Wallet download address:',
            '<div class="code"><a href="http://www.mathwallet.org" target="_blank">http://www.mathwallet.org</a></div>',
            'Awake Wallet tutorial:',
            '<div class="code"><a href="https://mp.weixin.qq.com/s/VC4uwNO99Cttj3k_5lEOgg" target="_blank">https://mp.weixin.qq.com/s/VC4uwNO99Cttj3k_5lEOgg</a></div>',
            'Awake Wallet download address:',
            '<div class="code"><a href="https://www.eosawake.io/" target="_blank">https://www.eosawake.io/</a></div>',
            'EOSOU Wallet download address:',
            '<div class="code"><a href="https://bbs.eosforce.io/?thread-333.htm" target="_blank">https://bbs.eosforce.io/?thread-333.htm</a></div>',
          ]
        },
        {
          id: 2.3,
          title: 'Which exchanges support EOSC?',
          list: [
            'Centralized Exchange: Bitforex https://bitforex.com',
            'Decentralized Exchange: Akdex https://akdex.io (Math wallet and Awake wallet have built-in Akdex Exchange)',
            'OTC Exchange: EOSOU Wallet https://bbs.eosforce.io/?thread-333.htm',
          ]
        }
      ]
    },
    {
      id: 3,
      is_close: false,
      title: 'About voting rewards sharing',
      list: [
        {
          id: 3.1,
          title: 'How to get voting rewards?',
          list: [
            'https://mp.weixin.qq.com/s/6fdXZNGc2_CgdPpWj5GHwA'
          ]
        },
        {
          id: 3.2,
          title: 'How do the coins of the voting rewards sharing come out?',
          list: [
            'The EOSForce mainnet generates one block every 3s, each block produces 3 EOSC, and it produces about 31.53 million EOSC every year. 70% of the total output goes into the nodes rewards pool based on the voting weights. The nodes distribute the rewards to users according to the rewards ratio they set and the users’ voting weight.',
            'For example, the rewards ratio set by node mathwalletbp is 99.99%, which means that mathwalletbp pays 99.99% of its rewards pool to the users as voting rewards sharing, and the remaining 0.01% enters its node account.',
            'Voting annualized returns = (profitable node annual rewards *70% * Percentage of votes * dividend ratio) / number of total votes of the node * 100%'
          ]
        },
        {
          id: 3.3,
          title: 'How to participate in voting to obtain rewards and get votes redeemed?',
          list: [
            'Click "vote" on the right side of the tab of nodes to participate in voting and get rewards. Click "redemption" to modify and withdraw the votes.',
            'Each time you vote and redeem, you need to pay 0.05EOSC as a handling fee. There is a 3-days lock-up period for redemption, and shall be unlocked and claimed manually 3 days later.',
            'Click “Dividends to be claimed” on the tab of “My Votes” to receive rewards. Each time you claim the rewards, you need to pay a fee of 0.03 EOSC. A sufficient balance must be set aside as a handling charge before voting.',
          ]
        }
      ]
    },
    {
      id: 4,
      is_close: false,
      title: 'About nodes',
      list: [
        {
          id: 4.1,
          title: 'How to become an EOSForce Block Producer?',
          list: [
            'EOSForce mainnet block producer deployment tutorial',
            'CN : https://eosforce.github.io/Documentation/#/en/eosforce_bp',
            'EN: https://eosforce.github.io/Documentation/#/en-us/eosforce_bp'
          ]
        },
        {
          id: 4.2,
          title: 'How many block producers in EOSForce community?',
          list: [
            'There are more than 100 nodes in EOSForce community, among which the top 23 nodes with votes generate blocks and are called block producers. A node that receives more than 0.5% of the total votes of the current network is a profitable node.',
          ]
        },
        {
          id: 4.3,
          title: 'The income source of the block producers?',
          list: [
            'The initial circulation of EOSC is 1 billion',
            'One block generated per 3s and 3 EOSC in one block.',
            'Daily rewards of profitable node = 3 * 20 * 60 * 24 = 86,400 EOSC',
            'Annual rewards of profitable node = BP daily rewards * 365 = 31,536,000 EOSC',
            'The reward distribution is as follows:',
            'The nodes whose votes obtained are more than 0.5% of the total votes of current network are profitable nodes. Among these profitable nodes, there are BPs and BP candidates. The first 23 are BPs and the rest are BP candidates.',
            'For the BPs, 15% of the income will be credited to its account as basic salary, and 15% of the income will be credited to its account as block-generating reward. And the remaining 70% of the income will go into its account and rewards pool according to the allocation proportion set by the node.',
            'For the BP candidates, 15% of the income will be credited to its account as basic salary, and 15% of the income will be credited to EOSForce Foundation account Devfund (This account belongs to the community and is used to support the developers. The transaction requires a multi-sign of more than two-thirds nodes) .The remaining 70% of the income will go into its account and rewards pool according to the allocation proportion set by the node.'
          ]
        },
        {
          id: 4.4,
          title: 'Is voting rewards the same for different nodes?',
          list: [
            'Vote for nodes with different annualized interest rates, the voting rewards will be different. Users can vote for different nodes according to the annual interest rate and the contribution made by the node.',
          ]
        }
      ]
    },
    {
      id: 5,
      is_close: false,
      title: 'Lockup plan',
      list: [
        {
          id: 5.1,
          title: 'Why is there less EOSC in the account than at the time of mapping?',
          list: [
            'According to the proposal of EOSForce mainnet unactivated genesis account locking plan, on December 20, the unactivated EOSIO genesis accounts are locked with 80% of EOSC tokens, and replaced for the same amount of lock tokens. The remaining 20% can be freely used.'
          ]
        },
        {
          id: 5.2,
          title: 'Why lock up 80% EOSC of the account?',
          list: [
            'Early builders of EOSForce community believe that a large number of unactivated EOSC are not conducive to the future development and construction of the EOSForce mainnet. If these unactivated EOSC are activated after the EOSForce mainnet matures, it is unfair to community builders who have invested time and effort early. In response to this topic, there was a long and repeated discussion in the community, and on November 29, 2018, voted by the block producers and finally approved by 20 votes, 2 votes against, 1 abstention, passed the lock-up proposal jointly proposed by Awake, jiqix, eosou.io and walianwang. On December 20, 2018, EOSForce officially executed the unactivated account EOSC token lock-up contract.'
          ]
        },
        {
          id: 5.3,
          title: 'Is it possible to unfreeze the locked EOSC in the future?',
          list: [
            'There is no suitable solution in the community for how to deal with locked EOSC currently. Members of the community can present their opinions and suggestions to the community and nodes.'
          ]
        }
      ]
    },
    {
      id: 6,
      is_close: false,
      title: 'The EOSForce DAPP ecosystem',
      list: [
        {
          id: 6.1,
          title: 'Are there any DAPP on EOSForce mainnet currently?',
          list: [
            'At present, there are Millionaire, Finger Guess, EOSForce Pixel, Super Card, Big Color Quiz, Smoke and Ashes and Waiting for you. It involves DAPP of public benefit, art, social and finance. In the future, we will develop more DAPPs and build a complete EOS ecological community.'
          ]
        },
        {
          id: 6.2,
          title: 'How can I participate in DAPP?',
          list: [
            'At present, you can participate in voting rewards sharing and various DAPP projects of the EOSForce mainnet via Math Wallet or Awake Wallet.'
          ]
        }
      ]
    },
    {
      id: 7,
      is_close: false,
      title: 'EOSForce resource model',
      list: [
        {
          id: 7.1,
          title: 'The Difference of resource model between EOSFORCE and EOS EMLG',
          list: [
            'CPU and NET resources are allocated based on fees, and RAM resources are allocated based on leases'
          ]
        },
        {
          id: 7.2,
          title: 'Why does the transfer charge a fee? What resources are you paying for?',
          list: [
            'In EOSForce mainnet, each operation needs to consume certain network resources. Because the total network resources are limited, in order to allocate resources fairly and reasonably, and avoid excessive abuse, the EOSForce mainnet has innovatively established a set of model for allocating CPU and NET resources based on handling fee. Under this model, users need to pay a certain fee for each operation to obtain CPU and ',
            'NET resources. For example, the following operations:',
            'Transferring: 0.01EOSC',
            'User Creation: 0.1EOSC',
            'Voting: 0.05EOSC ',
            'Redemption: 0.05EOSC',
            'Memory renting: 0.05EOSC',
            'Rewards Claiming: 0.03EOSC'
          ]
        },
        {
          id: 7.3,
          title: 'What is the RAM resources of EOSForce mainnet used for? How to obtain RAM resources of EOSForce mainnet? Can RAM resources be bought and sold?',
          list: [
            "RAM resources provide users and developers with a memory space for nodes to store data for contract’s reading and writing. The RAM resources in EOSForce mainnet can only be obtained by means of leasing. The upper limit of the RAM can be used by the account is set according to the rent paid by the account. It can only be rented and cannot be bought or sold. In order to facilitate the user's operation, the EOSForce mainnet set an 8kb rent-free quota for each account at this stage, so most ordinary users do not need to care about RAM. Usually, developers need to pay rent for the RAM used by their DApp, including the RAM occupied by the contract and the RAM data generated in the execution.",

          ]
        },
        {
          id: 7.4,
          title: 'What to do with the handling fee and RAM resources rent?',
          list: [
            "Handling fee: The handling fee currently incurred for each transaction will be destroyed. Rent: EOSForce mainnet has implemented the function of paying rent for RAM resources by voting rewards. The user votes with a certain amount of EOSC to get the corresponding amount of RAM resources, and pays the rent of the RAM resources with the voting rewards. Currently 1KB RAM resources can be obtained by voting with 10EOSC. The rent will be returned to the rewards pool of the block producers in proportion to the dividend, and will finally return to the voting users."
          ]
        }
      ]
    }
  ],
  cn: [
    {
      id: 1,
      is_close: false,
      title: '关于EOSFORCE主网',
      list: [
        {
          id: 1.1,
          title: 'EOS原力介绍',
          list: [
            'EOSFORCE致力于在实践中探索更开放的加密经济基础设施。通过持续开发多链架构区块链协议，满足加密经济的多元化共识需求，推动区块链技术在各个领域的应用。',
            '目前已完成并上线EOSIO生态最活跃的平行主网EOSForce.io, 为用户提供更友好的资源模型，为开发者提供更丰富的区块链底层支持，为社区提供更公平的治理机制。用户可使用映射过的EOSIO私钥离线签名激活EOSForce.io主网资产。'
          ]
        },
        {
          id: 1.2,
          title: '什么是EOSC？EOSC是空投吗？',
          list: [
            'EOSC是原⼒社区启动的EOS主⽹的代币名称，代表EOSForce主链上的资产，可⽤于投票分红，出块，转账，也可以⽤于交易所交易等操作。 ',
            'EOSC不是空投，用户可使用北京时间2018年6月03日映射过的EOSIO私钥通过离线签名激活EOSC账户资产。'
          ]
        },
        {
          id: 1.3,
          title: 'EOSC和EOS有什么区别？',
          list: [
            'EOSC是原力启动的EOS主网的代币名称，EOS是EMLG启动的EOS主网的代币名称，这两种资产是不一样的，目前akdex.io交易所开通了EOS和EOSC的交易对。'
          ]
        },
        {
          id: 1.4,
          title: 'EOSC总量有多少？（答内涉及锁仓，及剩余分配情况）',
          list: [
            'EOSC初始发⾏量为 10 亿，为奖励23个节点，每年增发9460万个EOS COIN。 给 BlockOne 团队设置奖励每10年解锁约 1 亿 EOS，10年以后继续解锁，永不间断。',
            '据《EOSForce主网未激活创世账户的代币锁仓提案》，于12.20日将未激活的EOSIO创世账户的80%代币进行锁仓，等额置换为锁仓代币，剩余的20%可自由使用。每年增发的token也将依据主网现有EOSC总量进行适当调整。',
            '目前主网锁定EOSC的数量为672,390,243，剩余EOSC的总量为327,609,757'
          ]
        },
        {
          id: 1.5,
          title: 'EOSFORCE主网和EMLG主网有何区别？',
          list: [
            'EOS主⽹并不⽌⼀条，⽬前EOS社区启动了多条主⽹。由EOS原⼒社区启动的主⽹称为EOSFORCE主网，和由EMLG启动的主⽹是平⾏关系。 ',
            '我们在调试EOS软件的时候，发现了很多不稳定的因素，存在⼀定的资产安全⻛险。由于EOS本身是⼀套开源软件，官⽅并不保证软件所启动的区块链的安全性和稳定性。需要有 ⼈能够针对性测试和开发，维护整条链的。所以我们团队针对EOSIO的每个功能进⾏严密测试，针对官⽅代码进⾏升级优化，优先保证整条链的安全和稳定性，并于2018年6⽉22⽇正式启动EOSFORCE主⽹。',
            '新增了投票分红的激励创新，改进了一票一投的去中心化，收取交易手续费防DDOS，23个超级节点优化治理社区，引进新的资源模型等。'
          ]
        },
        {
          id: 1.6,
          title: 'EOSFORCE主网为何要将一票30投改为一票一投？',
          list: [
            '一票三十投会造成超级节点之间结盟进行相互投票，严重影响区块链网络的去中心化特性，这样的垄断给EOS社区带来了很大的伤害，而EOSFORCE主网的一票一投则有效地避免了这一情况的发生，可防止主网落入卡塔尔联盟的集中治理。',
            'Block One CEO Brendan Blumer 19年1月在电报群里也建议将EOSIO改为一票一投。',
          ]
        },
        {
          id: 1.7,
          title: '这个EOSC是EOSClassic吗？',
          list: [
            'EOSC的全称是EOS COIN，是EOSForce主网代币的简写，不是EOSClassic。'
          ]
        }
      ]
    },
    {
      id: 2,
      is_close: false,
      title: '关于创建原力账户和进行交易',
      list: [
        {
          id: 2.1,
          title: '如何创建原力账户？',
        },
        {
          id: 2.11,
          title: 'EOS创世用户：在以下钱包导入映射时获得的EOS私钥，即可拥有原力账户。',
          list: [
            '原力官方pc版钱包 — eosforce.io',
            '国际知名钱包 — scatter',
            '国际知名app钱包 — 麦子钱包',
            'eos生态知名app钱包 — awake'
          ]
        },
        {
          id: 2.12,
          title: 'EOS非创世用户：在以下钱包可以创建原力账户',
          list: [
            '原力官方pc版钱包 — eosforce.io',
            '国际知名app钱包 — 麦子钱包',
            'eos生态知名app钱包 — awake',
          ]
        },
        {
          id: 2.2,
          title: '哪些钱包支持EOSC？',
          list: [
            'scatter钱包激活教程：',
            '<div class="code"><a href="https://bbs.eosforce.io/topic/80/" target="_blank">https://bbs.eosforce.io/topic/80/</a></div>',
            'scatter钱包下载地址： ',
            '<div class="code"><a href="https://get-scatter.com/">https://get-scatter.com/</a></div>',
            'EOSForce钱包使用教程：',
            '<div class="code"><a href="https://mp.weixin.qq.com/s/jbRRAmhoCKgHlga2V7xsDw">https://mp.weixin.qq.com/s/jbRRAmhoCKgHlga2V7xsDw</a></div>',
            'EOSForce钱包下载地址：',
            '<div class="code"><a href="https://www.eosforce.io/">https://www.eosforce.io/</a></div>',
            '麦子钱包使用教程：',
            '<div class="code"><a href="https://mp.weixin.qq.com/s/QV0gQYfWV6xVIXpmtxpDOg">https://mp.weixin.qq.com/s/QV0gQYfWV6xVIXpmtxpDOg</a></div>',
            '麦子钱包下载地址：',
            '<div class="code"><a href="http://www.mathwallet.org">http://www.mathwallet.org</a></div>',
            'Awake钱包使用教程：',
            '<div class="code"><a href="https://mp.weixin.qq.com/s/VC4uwNO99Cttj3k_5lEOgg">https://mp.weixin.qq.com/s/VC4uwNO99Cttj3k_5lEOgg</a></div>',
            'Awake钱包下载地址：',
            '<div class="code"><a href="https://www.eosawake.io">https://www.eosawake.io/</a></div>',
            '欧柚钱包下载地址：',
            '<div class="code"><a href="https://bbs.eosforce.io/?thread-333.htm">https://bbs.eosforce.io/?thread-333.htm</a></div>',
          ]
        },
        {
          id: 2.3,
          title: '哪些交易所支持EOSC？',
          list: [
            '中心化交易所：币夫 https://bitforex.com ',
            '去中心化交易所：akdex https://akdex.io/（麦子钱包、awake钱包内嵌akdex交易所）',
            'OTC交易所：欧柚钱包 https://bbs.eosforce.io/?thread-333.htm',
          ]
        }
      ]
    },
    {
      id: 3,
      is_close: false,
      title: '关于投票分红',
      list: [
        {
          id: 3.1,
          title: '如何参与投票分红？',
          list: [
            'https://mp.weixin.qq.com/s/6fdXZNGc2_CgdPpWj5GHwA'
          ]
        },
        {
          id: 3.2,
          title: '用户投票分红的Coin从何而来？',
          list: [
            'Eosforce主网每 3s 出一个块，每个块产出3个EOSC，每年产出约3153万的EOSC，产出总量的70%依据投票权重进入节点分红池，节点依据其设置的分红比例和用户的投票权重分红给用户。',
            '例：mathwalletbp 节点设置的分红比例为99.99%，意为mathwalletbp将其分红池的99.99%作为给用户投票的分红回报，剩余的0.01%进入节点账户。',
            '投票年化利率 = （收益节点每年奖励*70%*得票率*分红比例）/ 所投节点总得票数 * 100%',
          ]
        },
        {
          id: 3.3,
          title: '如何参与投票领取分红及赎回投票？',
          list: [
            '点击节点一栏右侧 “开始投票” 即可参与投票获得分红，点击 "修改投票” 即可修改及撤回投票',
            '每次投票和赎回需0.05EOSC作为手续费，赎回有3天锁定期，3天后需手动解锁领回',
            '点击我的投票一栏即可领取分红，每次领取分红需0.03EOSC的⼿续费，投票前要预留足够的余额作为手续费',
          ]
        }
      ]
    },
    {
      id: 4,
      is_close: false,
      title: '关于节点',
      list: [
        {
          id: 4.1,
          title: '如何成为EOS原力超级节点？',
          list: [
            'EOSForce主网超级节点部署教程',
            '中文：https://eosforce.github.io/Documentation/#/zh-cn/eosforce_bp',
            '英文：https://eosforce.github.io/Documentation/#/en-us/eosforce_bp',
          ]
        },
        {
          id: 4.2,
          title: 'EOSForce有多少超级节点？',
          list: [
            'EOSForce有100多个节点，其中得票数前23名的节点产生区块并称之超级节点；得票数超过当前全网投票总数0.5%的节点称为收益节点。',
          ]
        },
        {
          id: 4.3,
          title: '超级节点的收益来源？',
          list: [
            '链初始发行量为 10 亿 EOSC',
            '每 3s 一个块，每个块3个eosc',
            '收益节点每日奖励 = 3 * 20 * 60 * 24 = 86，400 EOSC',
            '收益节点每年奖励 = 超级节点每日奖励 * 365 = 31,536,000 EOSC',
            '这部分奖励分配如下：',
            '得票数前23名的超级节点，收益的15%作为基本工资进入其账户,收益的15%作为出块奖励进入其账户，剩余的70%根据节点设置的分红比例分别进入节点账户和分红池。',
            '得票数超过当前全网投票总数0.5%，但不是超级节点的节点，收益的15%作为基本工资进入其账户,收益的15%进入原力社区开发者委员会账户：devfund（该账户归属于社区，用于支持开发者发展，转账需要2/3及以上节点多签才能执行）剩余的70%根据节点设置的分红比例分别进入节点账户和分红池。'
          ]
        },
        {
          id: 4.4,
          title: '投票给不同节点的收益一样吗？',
          list: [
            '投给不同年化利率的收益节点，收益就会不一样； 用户可以根据年华利率，节点所做贡献给不同节点投票'
          ]
        }
      ]
    },
    {
      id: 5,
      is_close: false,
      title: '冻结',
      list: [
        {
          id: 5.1,
          title: '为什么我账户的EOSC会比映射时少？',
          list: [
            '据《EOSForce主网未激活创世账户的代币锁仓提案》截止12.20，将未激活的EOSIO创世账户的80%代币进行了锁仓，等额置换为锁仓代币，剩余的20%可自由使用。'
          ]
        },
        {
          id: 5.2,
          title: '为何要将我账户的80%EOSC进行锁仓？',
          list: [
            'EOSForce主网社区的早期建设者们认为，大量未激活的EOSC不利于EOSForce主网未来的发展和建设。如果这些未激活的EOSC在EOSForce主网成熟后再激活，对早期投入了时间和精力的社区建设者们是一种不公平。针对这一话题，在社区内进行了长时间的反复讨论，最终于2018年11月29日，由超级节点投票表决，最终以20票赞同，2票反对，1票弃权通过了Awake、jiqix、eosou.io和walianwang联合提出的EOS原力主网未激活创世账号的代币锁仓计划并于2018年12月20日，正式执行未激活账户代币锁仓系统合约。'
          ]
        },
        {
          id: 5.3,
          title: '未来是否有可能解冻已锁仓的EOSC？',
          list: [
            '对于如何处置已锁仓的EOSC，目前社区内尚未有合适的解决方案。社区内的成员可以向社区和节点提出自己的观点和建议。'
          ]
        }
      ]
    },
    {
      id: 6,
      is_close: false,
      title: '原力DAPP生态',
      list: [
        {
          id: 6.1,
          title: 'EOSFORCE主网目前有哪些DAPP？',
          list: [
            '目前有大富豪，FINGER GUESS，原力像素游戏，Super Card，花色大竞猜，强堵灰飞烟灭，Waiting For You等涉及公益，艺术，社交，金融的DAPP，未来我们会开发更多的DAPP，建设一个完整的EOS生态社区。'
          ]
        },
        {
          id: 6.2,
          title: '去哪里可以参与DAPP？',
          list: [
            '目前麦子钱包和Awake钱包都可以参与EOS原力主网投票分红及各类基于EOS原力主网的DAPP项目。'
          ]
        }
      ]
    },
    {
      id: 7,
      is_close: false,
      title: '原力资源模型',
      list: [
        {
          id: 7.1,
          title: 'EOSFORCE主网的资源模型和EOS EMLG主网有何区别？',
          list: [
            '基于手续费分配cup和net资源，基于租赁分配RAM资源'
          ]
        },
        {
          id: 7.2,
          title: '转账为什么需要手续费，是在为什么资源付费？',
          list: [
            '在EOSForce主网中，每一笔操作都需要消耗一定的网络资源。由于网络资源总量有限，为了资源分配公平合理，不会被无度滥用，EOSForce主网创新性地建立了一套基于手续费分配CPU和NET资源的模型。在该模型下，用户需要为每一笔操作支付一定的手续费，以获得CPU和NET资源。例如以下常规操作：',
            '转账：0.01EOSC     创建用户：0.1EOSC',
            '投票：0.05EOSC     赎回投票：0.05EOSC  ',
            '租赁内存：0.05EOSC   领取分红：0.03EOSC',
          ]
        },
        {
          id: 7.3,
          title: '原力主网的RAM资源有什么用，如何获得原力主网的RAM资源，RAM资源可以进行买卖吗？',
          list: [
            "RAM资源可以为用户和开发者提供节点的内存空间，以存储供合约读写的数据。EOSForce主网中的RAM资源只可采用租赁的方式获得，以账户为单位根据其所缴纳租金来设置账户可以使用的RAM的上限，只能租赁使用，无法进行买卖。",
            "为了方便用户操作，现阶段原力主网为每个账户设置了8kb的免租金额度，这样对于绝大多数普通用户是不需要关心RAM的，通常，开发者需要为其DApp所使用的RAM支付租金，包括了合约占据的RAM和执行中产生的RAM数据。'"
          ]
        },
        {
          id: 7.4,
          title: '手续费和RAM资源的租金去向是？',
          list: [
            '手续费：目前每一笔交易产生的手续费都会被销毁。',
            '租金：EOSForce主网已经实现了通过投票获得的分红来支付RAM资源租金的功能。用户通过投票一定金额的EOSC获得对应数量的RAM资源，并以投票获得的分红来支付所租用的RAM资源的租金。目前这一费率为投票10EOSC获得1KB RAM资源。租金会按分红比例返还到出块节点的奖励池，最终返回到投票用户手上。',
          ]
        }
      ]
    }
  ],
  kr: [
    {
      id: 1,
      is_close: false,
      list: [ 'EOSForce 메인넷 관련',
        {
          id: 1.1,
          title: 'EOSForce 소개',
          list: [
            'EOSFORCE는 보다 개방적인 암호 경제 인프라를 탐구하는 데 전념하고 있습니다. 멀티 체인 아키텍처 블록 체인 프로토콜의 지속적인 개발을 통해 암호화 경제의 다원화 공감대 수요를 충족시키고, 다양한 분야에서의 블록체인 기술 적용을 촉진합니다.',
            'EOSIO 생태계에서 가장 활발하게 구동 중인 병렬 메인넷인 EOSForce.io는 사용자에게 보다 사용자 친화적인 자원 모델을 제공하고, 개발자에게 보다 풍부한 블록체인 기초 지원을 제공하며, 커뮤니티에는 보다 공정한 거버넌스 메커니즘을 제공합니다. 사용자는 매핑된 EOSIO의 Private Key 오프라인 서명을 사용하여 EOSForce.io 메인넷 자산을 활성화할 수 있습니다.'
          ]
        },
        {
          id: 1.2,
          title: 'EOSC란 무엇인가요? EOSC는 에어드랍 토큰인가요?',
          list: [
            'EOSC는 EOSForce 커뮤니티에 의해 시작된 EOSForce 메인넷의 토큰입니다. EOSC는 EOSForce 메인넷의 대표 자산으로 투표/보상, 블록 생성, 이체. 거래 등에 사용됩니다.',
            'EOSC는 에어드랍 토큰이 아닙니다. 사용자는 베이징 시간 기준 2018년 6월 3일 이전에 매핑된 EOSIO의 Private Key 오프라인 서명을 사용하여 EOSForce.io 메인넷 자산을 활성화할 수 있습니다.'
          ]
        },
        {
          id: 1.3,
          title: 'EOS와 EOSC는 어떻게 다른가요?',
          list: [
            'EOSC는 EOSForce 커뮤니티가 런칭한 메인넷의 EOS 메인넷의 토큰이고, EOS는 EMLG가 런칭한 EOS 메인넷의 토큰입니다. 이 두 자산은 다릅니다. 현재 Akdex.io 거래소에서 EOS/EOSC 거래 페어를 지원하고 있습니다.'
          ]
        },
        {
          id: 1.4,
          title: 'EOSC 총량 (락업 계획 및 나머지 부분 할당)',
          list: [
            'EOSC의 초기 발행량은 10억개로, 23개 BP에 의해 매년 9460만개가 블록 생성 보상으로 발행되고 있습니다. 10년마다 약 1억개의 EOSC가 언락되어 BlockOne 팀에 인센티브로 제공되며, 앞으로 10년 후에도 계속 유지됩니다.',
            'EOSForce 메인넷 미활성화 자산 락업 계획은 2018년 12월 20일에 실행되었으며, 락업 계획 실행 전까지 미활성화된 EOSC 자산의 80%를 락업하여 동일한 양의 락업 토큰으로 대체되었습니다.',
            '현재 락업된 EOSC는 672,390,243개이며 나머지는 327,609,757개입니다.'
          ]
        },
        {
          id: 1.5,
          title: 'EOSForce 메인넷과 EMLG 메인넷은 무엇이 다른가요?',
          list: [
            'EOS 메인넷은 한 개가 아니며, 현재까지 여러 EOS 커뮤니티는 다수의 EOS 메인넷이 런칭되었습니다. EOSForce 커뮤니티에 의해 런칭된 메인넷을 EOSForce 메인넷이라 하며, EMLG에 의해 런칭된 메인넷과 평행 관계입니다.',
            '우리가 EOS 오픈 소스 소프트웨어를 디버깅하는 중 많은 불안정한 요소들을 발견했고, 자산에 위험성이 존재한다고 판단하였습니다. EOS 자체는 오픈 소스 소프트웨어이기 때문에 누구도 EOS 소프트웨어의 보안 및 신뢰성을 보증하지 않습니다. 그렇기에 블록체인 전체의 유지 및 보수를 위한 표적 개발 및 전담팀이 필요한 것입니다. 따라서 EOSForce 팀은 EOSIO의 각 기능에 대해 면밀히 테스트를 실시하고 코드를 업그레이드 및 최적화하여 블록체인 전체의 보안과 신뢰성을 높였고, 2018년 6월 22일 EOSForce 메인넷을 정식 출범하였습니다.',
            'EOSForce 메인넷은 투표 보상 인센티브 메커니즘, 1-Token-1-Vote, DDOS 공격 방지를 위한 트랜잭션 수수료 방식, 23개 BP의 커뮤니티 거버넌스 최적화, 사용자 친화적인 새로운 자원 모델 등을 도입했습니다. '
          ]
        },
        {
          id: 1.6,
          title: 'EOSForce 메인넷이 1-Token-30-Vote 방식을 1-Token-1-Vote으로 수정한 이유가 무엇인가요?',
          list: [
            '1-Token-30-Vote 방식은 BP 사이에서 서로 표 수를 지원하는 등의 카르텔 형성을 초래해 블록체인 분권화에 심각한 악영향을 가져올 수 있습니다. 1-Token-1-Vote는 이런 상황을 효과적으로 방지하고 메인넷에서 카르텔의 중앙집권적 지배 구조에 빠지는 것을 막을 수 있습니다.',
            'BlockOne의 CEO Brendan Blumer 또한 2019년 1월 텔레그램 채널에서 투표 방식을 1-Token-1-Vote로 변경하는 것을 제안하였습니다.',
          ]
        },
        {
          id: 1.7,
          title: '이 EOSC가 EOSClassic인가요?',
          list: [
            '아닙니다. EOSClassic은 ETH 메인넷 토큰으로 EOSForce 메인넷 자산인 EOSC와는 전혀 무관합니다.'
          ]
        }
      ]
    },
    {
      id: 2,
      is_close: false,
      title: 'EOSForce 계정 생성 및 트랜잭션 관련',
      list: [
        {
          id: 2.1,
          title: 'EOSForce 계정은 어떻게 만드나요?',
        },
        {
          id: 2.11,
          title: 'EOS 재네시스 유저의 경우, EOS Private Key를 아래 지갑에 임포트 하시고 맵핑하실 수 있습니다.',
          list: [
            'EOSForce 공식 지갑 (PC 버전) - EOSForce.io',
            '글로벌 유명 월렛 - Scatter',
            '글로벌 유명 모바일 월렛 - Math Wallet',
            'EOS 생태계 유명 월렛 – Awake Wallet'
          ]
        },
        {
          id: 2.12,
          title: 'EOS 非재네시스 유저의 경우, 아래 지갑에서 계정을 생성하실 수 있습니다.',
          list: [
            'EOSForce 공식 지갑 (PC 버전) - EOSForce.io',
            '글로벌 유명 모바일 월렛 - Math Wallet',
            'EOS 생태계 유명 월렛 – Awake Wallet'
          ]
        },
        {
          id: 2.2,
          title: '어떤 지갑에서 EOSC를 지원하고 있나요?',
          list: [
            'Scatter를 통한 EOSC 활성화 가이드:',
            '<div class="code"><a href="https://bbs.eosforce.io/topic/82/" target="_blank">https://bbs.eosforce.io/topic/82/</a></div>',
            'Scatter 다운로드 링크:',
            '<div class="code"><a href="https://get-scatter.com/" target="_blank">https://get-scatter.com/</a></div>',
            'EOSForce 지갑 가이드:',
            '<div class="code"><a href="https://mp.weixin.qq.com/s/jbRRAmhoCKgHlga2V7xsDw" target="_blank">https://mp.weixin.qq.com/s/jbRRAmhoCKgHlga2V7xsDw</a></div>',
            'EOSForce 지갑 다운로드 링크:',
            '<div class="code"><a href="https://www.eosforce.io/" target="_blank">https://www.eosforce.io/</a></div>',
            'Math Wallet 사용 가이드:',
            '<div class="code"><a href="https://mp.weixin.qq.com/s/QV0gQYfWV6xVIXpmtxpDOg" target="_blank">https://mp.weixin.qq.com/s/QV0gQYfWV6xVIXpmtxpDOg</a></div>',
            'Math Wallet 다운로드 링크:',
            '<div class="code"><a href="http://www.mathwallet.org" target="_blank">http://www.mathwallet.org</a></div>',
            'Awake 지갑 사용 가이드:',
            '<div class="code"><a href="https://mp.weixin.qq.com/s/VC4uwNO99Cttj3k_5lEOgg" target="_blank">https://mp.weixin.qq.com/s/VC4uwNO99Cttj3k_5lEOgg</a></div>',
            'Awake 지갑 다운로드 링크:',
            '<div class="code"><a href="https://www.eosawake.io/" target="_blank">https://www.eosawake.io/</a></div>',
            'EOSOU 지갑 다운로드 링크:',
            '<div class="code"><a href="https://bbs.eosforce.io/?thread-333.htm" target="_blank">https://bbs.eosforce.io/?thread-333.htm</a></div>',
          ]
        },
        {
          id: 2.3,
          title: '어떤 거래소에서 EOSC 거래를 지원하고 있나요?',
          list: [
            '중앙화 거래소: Bitforex https://bitforex.com',
            '탈중앙화 거래소: Akdex https://akdex.io  (Math wallet과 Awake 지갑에서 지원하고 있습니다.)',
            'OTC 거래소: EOSOU Wallet https://api.lend0x.pro/agreement/registerBank',
          ]
        }
      ]
    },
    {
      id: 3,
      is_close: false,
      title: '투표 보상 관련',
      list: [
        {
          id: 3.1,
          title: '어떻게 투표 보상을 받을 수 있나요?',
          list: [
            'https://mp.weixin.qq.com/s/6fdXZNGc2_CgdPpWj5GHwA'
          ]
        },
        {
          id: 3.2,
          title: '투표 보상은 어떻게 분배되나요?',
          list: [
            'EOSForce 메인넷은 3초에 한 개씩 블록을 생성하며, 매 블록당 3 EOSC가 발행이 됩니다. (1년에 31,530,000개가 발행이 됩니다.) 발행된 EOSC 중 70%는 노드의 보상풀로 들어가게 되며, 보상풀에서 노드가 설정한 분배 비율에 따라 사용자들에게 분배됩니다. (보상풀로 들어간 70%에서 노드가 임의로 분배 비율을 정할 수 있습니다.)',
            '예를 들어, mathwalletbp의 경우 배당 비율이 99.99%인데, 이는 보상풀 수량 중 99.99%를 사용자들에게 분배하며, 0.01%를 노드가 가져감을 의미합니다.',
            '연간 수익 = (노드 연간 수익 * 70% * 득표율 * 배당 비율 / 전체 투표 참여 수량) * 100%'
          ]
        },
        {
          id: 3.3,
          title: '투표 배당 수령 방법 및 투표 언스태이킹 방법',
          list: [
            '오른쪽에 있는 ‘투표’ 탭에서 투표하고 배당을 수령하실 수 있습니다. 행사한 표를 철회하시려면 언스태이킹을 하셔야 합니다.',
            '언스태이킹에는 72시간이 소요되며 0.05 EOSC의 수수료가 필요합니다. 72시간 후에는 수동으로 언스태이킹을 확인하셔야 합니다.',
            '배당금 수령의 경우 ’나의 투표’ 탭에서 수령하실 수 있으며, 수령 시 0.03 EOSC의 수수료가 필요합니다.투표 전 수수료로 사용할 충분한 잔고를 남겨두셔야 합니다.',
          ]
        }
      ]
    },
    {
      id: 4,
      is_close: false,
      title: '노드 관련 ',
      list: [
        {
          id: 4.1,
          title: '어떻게 EOSForce BP가 될 수 있나요?',
          list: [
            'EOSForce 메인넷 BP 생성 가이드',
            '중국어: https://eosforce.github.io/Documentation/#/en/eosforce_bp',
            '영어: https://eosforce.github.io/Documentation/#/en-us/eosforce_bp'
          ]
        },
        {
          id: 4.2,
          title: 'EOSForce 생태계에는 몇 개의 BP가 있나요?',
          list: [
            'EOSForce에는 100개 이상의 커뮤니티 노드가 참여 중이며, 이 중 상위 23개 노드가 블록을 생성하고 수익이 발생하며, 이 노드들을 BP라고 합니다. BP외에도 전체 투표 수량의 0.5% 이상의 표를 투표 받은 노드의 경우 수익이 발생합니다'
          ]
        },
        {
          id: 4.3,
          title: 'BP의 수입은 어떻게 되나요?',
          list: [
            '초기 발행량은 10억 개입니다.',
            '3초에 블록이 한 개씩 생성되며, 매 블록마다 3 EOSC가 발행됩니다.',
            '수익 노드의 일일 수입: 3 * 20 * 60 * 24 = 86,400 EOSC',
            '수익 노드의 연간 수입: 일일 수입 * 365 = 31,536,000 EOSC',
            '분배 방식',
            '상위 23개 노드(BP): 수입의 15%가 기본 급여로 노드 계정으로 들어가고, 추가로 15%가 블록 생성 보상으로 노드 계정으로 들어갑니다. 나머지 70%는 노드가 정한 분배 비율에 따라 노드 계정과 보상풀로 들어갑니다.',
            '非BP의 노드 수익(전체 투표 수량의 0.5% 이상 득표한 노드): 수입의 15%가 기본 급여로 노드 계정으로 들어갑니다. 70%는 노드가 정한 분배 비율에 따라 노드 계정과 보상풀로 들어갑니다. 나머지 15%는 EOSForce Foundation 계정으로 들어갑니다.',
            '(EOSForce Foundation 계정: Devfund, 이 계정은 커뮤니티 계정으로 계발자 지원에 사용됩니다. 이 계정의 자금 사용은 2/3 이상 노드의 멀티 사인이 필요합니다.)'
          ]
        },
        {
          id: 4.4,
          title: '모든 노드의 투표 보상이 동일한가요?',
          list: [
            '배당 비율에 따라 투표 보상 액수가 달라집니다. 사용자는 노드의 연간 이자율이나 해당 노드의 EOSForce 생태계 기여율을 보고 판단하여 서로 다른 노드에 투표하실 수 있습니다.',
          ]
        }
      ]
    },
    {
      id: 5,
      is_close: false,
      title: '락업',
      list: [
        {
          id: 5.1,
          title: '자신의 계정의 EOSC가 재네시스 스냅샷 수량보다 적은 이유',
          list: [
            'EOSForce 메인넷 미활성화 자산 락업 계획이 2018년 12월 20일에 실행되었으며, 락업 계획 실행 전까지 미활성화된 EOSC 자산의 80%를 락업하여 동일한 양의 락업 토큰으로 대체되었습니다.'
          ]
        },
        {
          id: 5.2,
          title: '락업이 진행된 이유가 무엇인가요?',
          list: [
            'EOSForce 메인넷 커뮤니티의 초기 참여자들은 다량의 미활성화 EOSC가 EOSForce 메인넷의 미래 개발과 발전에 도움이 되지 않는다고 판단했습니다. EOSForce 메인넷이 훗날 크게 성장한 이후, 이 많은 수량의 EOSC가 활성화될 경우 초기에 시간과 노력을 투자한 커뮤니티 초기 참여자들에게는 불공평하다고 생각될 것입니다. 이 주제에 대해 커뮤니티에서 많은 논의가 있었고, 2018년 11월 29일, 최종적으로 BP회의에서 Awake, Jiqix, EOSOU가 제안한 락업 제안에 대해 찬성 20표, 반대 2표, 기권 1표가 나와 락업 제안이 승인되었습니다. 락업은 2018년 12월 20일 공식적으로 진행되었습니다.'
          ]
        },
        {
          id: 5.3,
          title: '락업된 EOSC는 향후 어떻게 되나요?',
          list: [
            '락업된 EOSC를 어떤 방법으로 처리할 것인지에 대해 현재까지 커뮤니티 내에서 적절한 방안이 결정되지 않았습니다. 커뮤니티 내 참여자들은 자유롭게 의견과 제안을 낼 수 있습니다.'
          ]
        }
      ]
    },
    {
      id: 6,
      is_close: false,
      title: 'EOSForce DAPP',
      list: [
        {
          id: 6.1,
          title: 'EOSForce 메인넷에는 어떤 DAPP들이 있나요?',
          list: [
            '현재, Millionaire, Finger Guess, EOSForce Pixel, Super Card, Big Color Quiz, Smoke and Ashes, Waiting for you 등의 DAPP이 있습니다. DAPP에는 겜블링, 예술, 사회, 경제 분야 등이 포함되어 있으며, EOSForce는 더 많은 DAPP 지원을 통해 성숙한 EOS 생태 커뮤니티를 위해 노력하고 있습니다.'
          ]
        },
        {
          id: 6.2,
          title: '어떻게 DAPP들을 이용할 수 있나요?',
          list: [
            'Math Wallet과 Awake 지갑에서 DAPP들을 지원하고 있습니다.'
          ]
        }
      ]
    },
    {
      id: 7,
      is_close: false,
      title: 'EOSForce 자원 모델',
      list: [
        {
          id: 7.1,
          title: 'EOSForce의 자원 모델은 EMLG 메인넷과 어떤 차이가 있나요?',
          list: [
            'CPU and NET resources are allocated based on fees, and RAM resources are allocated based on leases'
          ]
        },
        {
          id: 7.2,
          title: '트랜잭션에 수수료가 붙는 이유와 리소스에 비용이 필요한 이유',
          list: [
            'EOSForce 메인넷에서 매 트랜잭션이 발생할 때마다 특정 네트워크 리소스를 필요로 합니다. 전체 네트워크 리소스는 제한적이기 때문에 공정하고 합리적인 분배와 과도한 남용을 막기 위해 EOSForce 메인넷에서는 수수료에 기초한 CPU와 NET 리소스 배분 모델을 혁신적으로 구축하였습니다. 이 모델에서 사용자는 CPU와 ',
            'NET을 사용하기 위해 각 작업에 대한 일정한 수수료를 부담해야 합니다.',
            '이체: 0.01EOSC',
            '계정 생성: 0.1EOSC',
            '투표: 0.05EOSC',
            '투표 언스태이킹: 0.05EOSC',
            '메모리 대여: 0.05EOSC',
            '투표 배당 수령: 0.03EOSC'
          ]
        },
        {
          id: 7.3,
          title: 'EOSForce 메인넷의 RAM 리소스는 어디에 쓰이고, 어떻게 얻으며, 매매가 가능한가요?',
          list: [
            "RAM 리소스란, 컨트랙트의 읽기 및 쓰기를 위한 데이터를 저장할 수 있는 메모리를 뜻하며 노드가 사용자와 개발자에게 제공합니다. EOSForce 메인넷의 RAM 리소스는 매매가 불가능하고, 임대를 통해서만 얻을 수 있으며 지불한 임대료에 따라 RAM 리소스 양이 정해집니다. EOSForce는 사용자 친화성을 향상시키기 위해 각 계정마다 8KB의 무료 임대를 할당해두었기 때문에 일반 사용자는 RAM에 대해 신경 쓰실 필요가 없습니다. 개발자의 경우, 점유한 RAM과 실행에서 생성된 RAM 데이터를 포함해 DAPP에서 사용된 모든 RAM에 대한 임대료를 지불해야 합니다.",

          ]
        },
        {
          id: 7.4,
          title: '수수료와 RAM 임대료는 어떻게 사용되나요?',
          list: [
            "수수료: 트랜잭션에서 발생한 모든 수수료는 소각됩니다.",
            "임대료: EOSForce 메인넷에서는 투표 배당 수익을 통해 RAM의 임대료가 지불되는 기능을 운영 중입니다. 사용자는 RAM 임대료를 지불하기 위해 일정량의 EOSC를 투표하고 여기서 나온 투표 배당 수익으로 임대료를 지불합니다. 현재 10 EOSC를 투표할 경우 1KB의 RAM 리소스를 얻을 수 있습니다. 임대료는 배당 비율에 따라 BP의 보상풀로 들어간 후 최종적으로 사용자에게 분배됩니다. "
          ]
        }
      ]
    }
  ],
}









