// http://www.eosforcegohome.com/App/GetMostInviteCount

import axios from 'axios'

const BASE_CONFIG = {
    // host: 'http://192.168.82.133:8888'
    host: 'https://explorer.eosforce.io'
}
export const get_most_invite = () => {
    return new Promise((resolve, reject) => {
        let url = BASE_CONFIG.host + '/App/GetMostInviteCount';
        axios.get(url)
        .then(res => {
            resolve(res);
        })
        .catch(e => {

        })
    });
}

export const get_moset_invite_assets = () => {
    return new Promise((resolve, reject) => {
        let url = BASE_CONFIG.host + '/App/GetMostInviteAsset';
        axios.get(url)
        .then(res => {
            resolve(res);
        });
    });
}

export const get_most_bounds = () => {
    return new Promise((resolve, reject) => {
        let url = BASE_CONFIG.host + '/App/GetMostBonus';
        axios.get(url)
        .then(res => {
            resolve(res);
        });
    });
}

export const get_bounds_summary = () => {
    return new Promise((resolve, reject) => {
        let url = BASE_CONFIG.host + '/App/GetBonusSummary';
        axios.get(url)
        .then(res => {
            resolve(res);
        });
    });
}

// http://132.232.22.135/App/GetInviteDetail?account=gqztcmrsgyge
export const get_invite_detail = (account) => {
    return new Promise((resolve, reject) => {
        let url = BASE_CONFIG.host + '/App/GetInviteDetail',
            params = {
                account
            }
        axios({
            url,
            method: 'GET',
            params
        })
        .then(res => {
            resolve(res);
        });
    });
}
