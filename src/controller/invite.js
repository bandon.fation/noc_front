import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);
import eos_invite from '../components/eos_invite/eos_invite.vue'

var app = new Vue({
  el: '#app',
  components:{
    eos_invite
  }
});