import vue from 'vue'
import vue_router from 'vue-router'
import Vuex from 'vuex'
vue.use(vue_router);
vue.use(Vuex);

import index_view from '@/components/concat/blog_index.vue'
import eos_blog from '@/components/eos_blog/eos_blog.vue'
import eos_blog_item from '@/components/eos_blog/eos_blog_item.vue'
import eos_blog_manage from '@/components/eos_blog/eos_blog_manage.vue'
import { check_lang_key } from '../data/lang.js'
const url_lang = check_lang_key();
const router_link = [
    {
        path: '/',
        redirect: '/' + url_lang,
        name: 'app',
        component: index_view,
        children: [
            {
                path: '/' + url_lang ,
                name: 'eos_blog',
                component: eos_blog
            },
            {
                path: '/' + url_lang + '/blog_id=:blog_id' ,
                name: 'eos_blog_item',
                // component: () => import('../components/eos_blog/eos_blog_item.vue')
                component: eos_blog_item
            },
            {
                path: '/' + url_lang + '/manage' ,
                name: 'eos_blog_manage',
                // component: () => import('../components/eos_blog/eos_blog_manage.vue')
                component: eos_blog_manage
            }
        ]
    }
];

const router_config = new vue_router({
    // mode: 'history',
    routes: router_link
});

const app = new vue({
    router: router_config
}).$mount('#app');

