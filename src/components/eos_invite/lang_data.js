let data =  {
    'EOS创世投资者': 'EOS Genesis Investors',
    '激活你的原力创世账号': {
        en: 'EOSForce Genesis Homecoming.',
        kr: '재네시스 계정을 활성화하고, 토큰을 받아 가세요!'
    },
    '即可获得激活奖励TOKEN': {
        en: 'Activate your genesis account for 100,000 EOSC',
        kr: 'Activate your genesis account for 100,000 EOSC'
    },
    '查看教程': 'View Scatter Instruction',
    '马上激活账户': 'Acitve Now',
    '欢迎回家！': 'Welcome Home!',
    '用户名': 'Your Account',
    '点击此处完成你的创世激活！': {
        en: 'Click here to finish your genesis activation!',
        kr: '이곳을 클릭하여 재네시스 계정을 활성화하세요.'
    },
    '分享到推特': {
        en: 'Share this to Twitter to get inviter\'s bonus',
        kr: '트위터에 공유하기'
    },
    '使用scatter钱包遇到问题？请点击下边按钮进入教程': {
        en: 'Learn how to configure EOSForce on Scatter',
        kr: 'Scatter 사용에 문제가 있으면 아래 Scatter 사용 설명을 참고하세요.'
    },
    'scatter使用EOS原力教程': {
        en: 'View Scatter Instruction',
        kr: 'EOS Force, Scatter 사용 설명'
    },
    '未安装scatter,点击安装': {
        en: 'View Scatter Instruction',
        kr: 'Scatter 설치하기'
    }, // 'install scatter to active your account',
    '转账中...': {
        cn: 'transfer...',
        kr: 'transfer...'
    },
    '填写推荐人账户(可不填)': {
        en: 'Your inviter\’s account name(optional)',
        kr: '초대 계정명을 입력하세요.(선택사항)'
    },
    '未填写推荐人': 'No Invitor',
    '马上激活': 'Active',
    '账号': 'account',
    '切换用户': 'exchange',
    '选择scatter中需要激活的账户': {
        en: 'choose user from scatter which you need to active',
        kr: 'Scatter에서 활성화하려는 계정으로 접속합니다.'
    },
    'https://bbs.eosforce.io/?thread-328.htm': {
        en: 'https://medium.com/@eosforce/how-to-configure-eosforce-mainnet-on-scatter-desktop-b8061966a59',
        kr: 'https://bbs.eosforce.io/?thread-331.htm'
    },
    '邀请人次': {
        en: 'success times',
        kr: '초대를 통해 활성화시킨 누적 계정 수 랭킹'
    },
    '邀请币数': {
        en: 'reward EOS coin',
        kr: '초대를 통해 활성화시킨 계정의 누적 EOSC 랭킹'
    },
    '暂无数据': 'no data',
    '为了进一步宣传EOSForce，推动EOS创世投资人尽快回归EOSForce，我们发起了这个“原力回家行动”。': 'To both further promote EOSForce as well as getting more EOS Genesis investors to join the EOSForce Mainnet, we are launching the “Bring Force Home” promotional event . ',
    '推广词': 'Promotion Word',
    '复制成功': 'Copy successful',
    '请输入您的账号': 'Please Enter Your Account',
    '推广我的账号': 'Promotion My Account',
    '我的推广账号': 'My promotion account',
    '输入我的账号，产生推广词': 'Enter EOSForce account name to generate your promotion word.',
    '拷贝推广词': 'Copy Promotion Word',
    '欢迎参加“原力回家行动”!': 'Come join us in “Bring Force Home”!',
    '活动期内，8月1日以后新激活的创世帐号(从ETH映射到原力的账户)，向checkin帐号转账0.0001 EOS Coin签到，留言附上推荐帐号': 'Throughout the promotion, any genesis accounts (accounts that were created through activation based on the EOS ERC20 snapshot) joining the EOSForce miannet after August 1st can receive EOS Coin rewards by simply activating their accounts on the EOSForce Mainnet.  Activation can be completed by transferring 0.0001 EOS Coin to the EOSForce account called “checkin”.  Once activation is successful, the user will receive a 10.24 EOS Coin reward.    Additionally, if the participant obtained the info from an existing EOSForce account holder, he/she can also enter the EOSForce ',
    '，双方可获10.24 EOS Coin激活奖和邀请奖！': '’s name in the transaction’s memo section.  Once the transaction goes through and the memo info is confirmed, the recommending party will receive 10.24 EOS Coin as well.',
    '活动期内，8月1日以后新激活的创世帐号(从ETH映射到原力的账户)，向checkin帐号转账0.0001 EOS Coin签到，留言附上推荐帐号，双方可获10.24 EOS Coin激活奖和邀请奖！': 'Throughout the promotion, any genesis accounts (accounts that were created through activation based on the EOS ERC20 snapshot) joining the EOSForce miannet after August 1st can receive EOS Coin rewards by simply activating their accounts on the EOSForce Mainnet.  Activation can be completed by transferring 0.0001 EOS Coin to the EOSForce account called “checkin”.  Once activation is successful, the user will receive a 10.24 EOS Coin reward.    Additionally, if the participant obtained the info from an existing EOSForce account holder, he/she can also enter the EOSForce user’s name in the transaction’s memo section.  Once the transaction goes through and the memo info is confirmed, the recommending party will receive 10.24 EOS Coin as well.',
    '所有上级推荐人，依次减半获得推荐奖！': 'Additionally, all top recommending parties will receive extra 50% EOS Coin rewards if the user they recommended starts to recommend others to join. ',
    '活动结束时更为最佳推荐人开出3万 EOS Coin的壕礼！': 'At the end of the promotion, the top recommender of all will receive another 30000 extra  EOS Coin!',
    '更多详情请访问www.eosforcegohome.com/eos_invite.html': 'For more details please visit: www.eosforcegohome.com/eos_invite.html！',
    '活动规则': ' Steps',
    '凡还未激活EOSForce的创世投资者，在活动期间，向帐号checkin发送0.0001 EOS Coin报到，以确认激活，即可获得10.24 EOS Coin的奖励。 注:创世账号查询地址 https://explorer.eosforce.io/#/eth_eos_mapping': 'Any EOS genesis account holders activating their accounts during the promotion period needs to send 0.0001 EOS Coin to the “checkin” account to ensure EOSForce account activation.  Once confirmed, the account holder can get 10.24 EOS Coin rewards. Note: to see check your genesis account status, please visit: https://explorer.eosforce.io/#/eth_eos_mapping',
    '新激活的EOSForce投资者在向帐号checkin发送0 EOS Coin时，如果留言中填写了推荐者帐号，则推荐者也可获得10.24 EOS Coin的奖励。 请仔细填写，只有第一次转账的留言被认可！': 'When sending 0.0001 EOS Coin to the “checkin” account, existing EOSForce users that recommended the prospective EOSForce users can also receive a 10.24 EOS Coin reward by having the prospective user entering the recommending party’s name in the transaction’s memo section.  Note the system will only recognize and approve memo info from the first transaction, therefore please be extra careful when entering the candidate’s account name. ',
    '如果被推荐者再次推荐激活成功，那么上级推荐者获得5.12 EOS Coin奖励，所有上级推荐者依次减半获得奖励。聪明的您，可以尝试链状 推荐、树状推荐、环状推荐、互相推荐，让奖金爆棚！': 'Additionally, if the recommended candidate starts successfully recommend other genesis account holders into joining the EOSForce Mainnet, the first EOSForce user will receive an additional 5.12 EOS Coin reward - half the amount of the 10.24 EOS Coin reward - without doing any work.  Therefore, as the users you recommended recommend more prospective users, you will also be rewarded from their recommendations!',
    '此活动筹备了12万EOS Coin的奖金，奖励发送完毕则活动提前结束。': 'For this promotion we have prepared 120,000 EOS Coins as rewards.  The promotion ends once all 120,000 EOS Coins have been rewarded.',
    '活动结束后，邀请人数榜、邀请币数榜、获奖最多榜，每个榜单前3名，将依次获得5000/3000/2000、合计30000的额外特别奖！': 'Once the promotion ends, the top 3 rankings from each of the 3 leaderboards (Most Users Recommended, Most Coins Received From Promotion, Most Awards Received) will each receive 5000/3000/2000 additional EOS Coins.',
    '累计发送奖金': {
        en: 'Total EOS Coins Rewarded',
        kr: '현재까지 지급된 누적 보상'
    },
    '邀请人数榜': {
        en: 'Activation reward',
        kr: '초대한 누적 계정 수'
    },
    '邀请币数榜': {
        en: 'Invitation bonus',
        kr: '초대한 계정의 누적 EOSC'
    },
    '获奖榜': {
        en: 'Invitation ranking bonus',
        kr: '지급받은 누적 보상'
    },
    '关于EOSForce': 'About EOSForce',
    '原力秉承比特币开放自由的精神，严守公平正义的底线，开发和运营活动没有进行过任何ICO和增发，矢志不渝为探索区块链而努力！': 'The Force community follows the Bitcoin spirt of being free and open as well as adhering to justice and fairness in its conduct.  Neither the development nor the operations of EOSForce engages in any ICO or additional coin offering activities. ',
    '目前原力最有特色的是投票分红，充分体现了投资者权利，有效激活了社区。经过投资者们口口相传，目前已有4500万币在投票，这一数字还在迅速增长中！': 'Currently the most well-known characteristic of EOSForce is its “dividend sharing through voting” feature.  This feature promptly realizes the investors’ right to dividends, and has greatly energized the EOS community.  With only word-of-mouth recommendations, the EOSForce Mainnet has received over 45,000,000 votes, and the number is still growing.',
    '除了投票分红，EOSForce对EOS的治理、安全、公正性、合理性等做了多方面改进。': 'In addition to the dividend-through-sharing feature, EOSFore has also improved the original EOS model in areas such as governance, safety, fairness, and reasonableness.',
    '比如，没有采用抵押或者购买资源的方式，而是采用简单易用的Gas模型，杜绝了资源暴涨暴跌对开发者和用户的影响。': 'For example, EOSForce uses the Gas model for transaction fees instead of the deposit/resource purchase model that is used by the original EOS model.  By doing this, we are able to avoid volatile changes in the supply of development resources as well as the volatility’s negative effect on both the developers and users.',
    '再如，在架构演进上，EOSForce的方案也非常安全高效。主链去掉了冗余功能，只进行结算，把智能合约放到侧链。这不仅提高了主链安全性，也为提升侧链效率打开了极大的想象空间。': 'Additionally, EOSForce has a highly effective and safe solution towards evolving the structure of the EOS blockchain.  For example, the EOSForce’s main chain keeps only the settlement function and has moved the smart contract features to the sidechains.  By doing this we not only increased the safety of the main chain, but also provided great potential for sidechains to improve their efficiency.',
    'EOSForce遵循EOS创世名单，认可创世投资者为EOS的诞生作出的巨大贡献，感谢大家为EOS的诞生承担的的巨大风险！也希望各位投资者能够认可EOSForce为改善整个EOS生态、提升EOS创世投资者价值所作出的巨大努力！': 'EOSForce adheres to the original EOS genesis list, and recognizes the enormous amount of contributions the investors has provided to the project.  The Force team is deeply appreciative of the trust the genesis investors had given to the EOS project.  We hope that the EOS investors will see and acknowledge the efforts  that EOSForce has provided in order to improve the entire EOS ecosystem as well as increasing the value of the EOS genesis investors.',
    '各位创世投资人登录www.eosforce.io，下载钱包，灌入私钥，即可按照传世名单份额申领EOSForce。': 'Visit www.eosforce.io, download our Force wallet, import your EOS private key into the wallet once the download is complete, and you will receive the equal amount of EOS Coin in accordance with the genesis snapshot.To register your EOSForce account and claim your EOS Coins, please follow the following method:',
    '如果有安全担心，建议如下操作：': 'If you are worried about the safety of existing EOS assets, we advise that you:',
    '1、首先在原链上，将原链的资产转移给小号。如果是专业人士，推荐在原链上修改账户绑定的公钥。': '1. Transfer your EOS tokens to to a separate account before importing your private key into the EOSForce wallet.  If you are experienced with EOS functions, we advise that you change the public key to your original EOS chain;',
    '2、随后，在EOSForce官方钱包，灌入私钥，申领EOSForce。同样建议在EOSForce上将币转给小号，确保安全。然后投票即可享受分红！': '2. Only input your private key into the official wallet which can be downloaded from the official EOSForce website (www.eosforce.io).  For further safety, we also advise that you transfer your EOS Coins to a separate Force account and vote from there.',

    '获取更多奖励': {
        en: 'GET INVITATION BONUS',
        kr: '더 많은 보상 받기'
    },
    
    '激活有奖':' Activation reward',
    '首次激活EOSForce账户，在活动期间向帐号checkin发送0 EOSC签到，即可获得200 EOSC的奖励。 注:创世账号查询地址 https://explorer.eosforce.io/#/eth_eos_mapping': 'Users that activate their genesis account, send 0 EOSC to the EOSForce account name “checkin” for a 200 EOSC activation reward. check your account at https://explorer.eosforce.io/#/eth_eos_mapping',
    '推荐有奖': ' Invitation bonus ',
    '激活账户B在转账留言中（memo）填写推荐者帐号A，则推荐者A可获得100 EOSC的奖励。（以第一次留言为准）': ' Activated account B writes the account name of the inviter A in the memo section of the transaction to “checkin”. The inviter’s account A receives an invitation bonus of 100 EOSC (the inviter record is set to the account name entered first time by the newly activated user). ',
    '如果被推荐者B继续推荐别人C激活成功，那么上级推荐者A继续获得50EOSC奖励。': ' If the inviter B invites user C to activate, inviter A continues to receive an affiliate invitation bonus of 50 EOSC.',
    '所有上级推荐者依次减半获得奖励，也就是50/25/12.5….，不限层级。': ' Upper level inviter receives an invitation bonus each time a new user activates, with the bonus reducing by half each time:50/25/12.5 etc. ',
    '邀请排行榜特别奖': ' Best inviters bonus',
    '邀请人数前3': ' Top 3 inviters with most invitations',
    '邀请激活账户币数前3': ' Top 3 accounts with the most activated token balance',
    '邀请激励前3': ' Top 3 invitation bonus amount inviters',
    '将依次获得5000/3000/2000，合计30000的额外特别奖！': 'Each of the reward inviters get an additional bonus of 5000/3000/2000 EOSC, totalling 30,000!'
}

export const get_lang = (key_word, lang_key = 'cn') => {
    let res = data[key_word];
    if(lang_key == 'cn'){
        return key_word;
    }

    if(typeof res == 'object'){
        return res[lang_key];
    }

    if(lang_key == 'en' || lang_key == 'kr'){
        if(!res){ return key_word; };
        return data[key_word];
    }else{
        return key_word;
    }
}


