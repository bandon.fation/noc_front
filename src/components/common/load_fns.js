import { log, time_plus_hours, is_mobile } from '../utils/utils.js'

export default {
    auto_update () {
        setTimeout(() => {
            if(this.page_index == 1){
                this.get_data(null, true);
            }
        }, 3000);
    },
    diff_data_timestamp (_time) {
        if(_time == this.load_timestamp) return true;
        return false;
    },
    new_data_timestamp () {
        this.data_timestamp = new Date().getTime();
        return this.data_timestamp;
    },
    next () {
        this.page_index++;
        this.page_index = this.page_index > this.page_num ? this.page_num : this.page_index;
        this.get_data(null, false);
    },
    pre () {
        this.page_index--;
        this.page_index = this.page_index <= 0 ? 1 : this.page_index;
        this.get_data();
    },
    create_page_list () {
        let max_page_num = 7;
        let start = this.page_index - 3 < 1 ? 1 : this.page_index - 3;
        let end = this.page_index + 3 > this.page_num ? this.page_num : this.page_index + 3;
        end = end - start < max_page_num - 1 ? end + (max_page_num + start - end - 1) : end;
        end = end > this.page_num ? this.page_num : end;
        this.page_list.splice(0, this.page_list.length);
        for (var i = start; i <= end; i++) {
            this.page_list.push(i);
        }
    },
    start_load (direct_load) {
        let load_timestamp = new Date().getTime();
        this.load_timestamp = load_timestamp;
        if(direct_load){
            this.on_load = true;
            return this.load_timestamp;
        }
        setTimeout(() => {
            if(this.load_timestamp == load_timestamp){
                this.on_load = true;        
            }
        }, 500);
        return this.load_timestamp;
    },
    finish_load () {
        this.load_timestamp = new Date().getTime();
        this.on_load = false;
    },
    check_load () {
        return this.on_load;
    },
    time_plus_hours,
    has_param_key_val () {
        if(this.$route.params[this.param_key] == '_'){
            return false;
        }
        return true;
    }
}